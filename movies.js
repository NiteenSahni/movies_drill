const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


// Q1. Find all the movies with total earnings more than $500M. 

function earnings(object) {
    return Object.entries(object).reduce((accu, movieInfo) => {
        if (Number(movieInfo[1]["totalEarnings"].replace('$', "").replace('M', "")) > 500) {

            accu[movieInfo[0]] = movieInfo[1]
        }
        return accu;
    }, {})
}


// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

function threeOscars(object) {
    return Object.entries(object).reduce((accu, movieInfo) => {
        if (Number(movieInfo[1]["totalEarnings"].replace('$', "").replace('M', "")) > 500 && movieInfo[1]["oscarNominations"] > 3) {

            accu[movieInfo[0]] = movieInfo[1]
        }
        return accu;
    }, {})
}



// Q.3 Find all movies of the actor "Leonardo Dicaprio".

function findLeo(object) {
    return Object.entries(object).reduce((accu, movieInfo) => {
        if (movieInfo[1]["actors"].toString().includes("Leonardo Dicaprio")) {

            accu[movieInfo[0]] = movieInfo[1]
        }
        return accu;
    }, {})
}



// Q.4 Sort movies (based on IMDB rating)
// if IMDB ratings are same, compare totalEarning as the secondary metric.

function sortMovies(object) {
    return Object.entries(object).sort((firstMovie, secondMovie) => {
        if (firstMovie[1]["imdbRating"] == secondMovie[1]["imdbRating"]) {
            return Number(firstMovie[1]["totalEarnings"].replace('$', "").replace('M', "")) - Number(secondMovie[1]["totalEarnings"].replace('$', "").replace('M', ""))
        } else {

            return firstMovie[1]["imdbRating"] - secondMovie[1]["imdbRating"]
        }
    }).reduce((accu, movieInfo) => {
        accu[movieInfo[0]] = movieInfo[1]
        return accu
    }, {})
}

